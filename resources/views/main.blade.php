@extends('main_master')
@section('section')
    <div class="row">
        <div class="col-md-12">
            <img src="img/flex.png" alt=""/>
            <div class="search">
                <div class="row">

                    <div class="col-md-12">
                        <div class="search">
                            <form action="" class="form-inline"
                            style="background-color: rgba(255,255,255, 0.5);
                            padding: 10px 40px;
                            position: absolute;
                            z-index: 9999; margin:-260px -37px 18px 340px;
                            width: 500px;" >
                                <input type="text" name="search" placeholder="Search"
                                style="width: 100%;
                                padding: 5px;
                                "/>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--catagory--}}
       @foreach($cat as $ca)
            <h4>{{$ca->name}}</h4>
            @foreach($room_detail = \App\Room::where('cat_id', $ca->id)->get() as $room_de)
              <div class="row">
                <div class="col-md-3">
                     <div class="cat-wrapper">
                            <img src="{{url('upload/'.$room_de->photo)}}" alt="" class="img-responsive" width="200px"/>
                     </div>
                </div>
              </div>
            @endforeach
       @endforeach
@endsection