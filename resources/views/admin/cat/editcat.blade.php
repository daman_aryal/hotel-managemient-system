@extends('......admin_master')
@section('section')
    <div class="jumbotron col-md-4" style="margin-top:10px; box-shadow: none">
        <h3>Add new Iteam</h3>
        <form action="{{url('cat/editcat')}}" method="post">
        {{csrf_field()}}
            <div class="form-group">
              <input type="hidden" name="id" value="{{$eData->id}}"/>
             <input type="text" name="cat_name" class="form-control"
             value="@if($errors->all()){{old('cat_name')}}@else{{$eData->name}}@endif"/>
             <span style="color: red; font-weight: lighter">{{$errors->first('cat_name')}}</span>
             <span style="color: red; font-weight: lighter">{{session('msg')}}</span>
            </div>
            <div class="form-group">
                <input type="text" name="cat_price" class="form-control"
                 value="@if($errors->all()){{old('cat_price')}}@else{{$eData->price}}@endif"/>
                 <span style="color: red; font-weight: lighter">{{$errors->first('cat_price')}}</span>
            </div>
            <div class="form-group">
              <textarea name="cat_description" id="" cols="30" rows="10" class="form-control text-left">
                @if($errors->all()){{old('cat_description')}}@else{{$eData->description}}@endif
              </textarea>
                 <span style="color: red; font-weight: lighter">{{$errors->first('cat_description')}}</span>
            </div>
            <input type="submit" value="Edit Catagory" class="btn btn-primary"/>

        </form>
    </div>
@endsection