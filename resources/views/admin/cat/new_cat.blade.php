@extends('......admin_master')
@section('section')
    <div class="jumbotron col-md-4" style="margin-top:10px; box-shadow: none">
        <h3>Add new Iteam</h3>
        <form action="{{url('cat/newcat')}}" method="post">
        {{csrf_field()}}
            <div class="form-group">
              <input type="text" name="name" class="form-control" placeholder="Catagory Name" value="{{old('name')}}"/>
              <span style="color: red; font-weight: lighter">{{$errors->first('name')}}</span>
            </div>
            <div class="form-group">
                <input type="text" name="cat_price" class="form-control" placeholder="Catagory Price" value="{{old('cat_price')}}"/>
                 <span style="color: red; font-weight: lighter">{{$errors->first('cat_price')}}</span>
            </div>
            <div class="form-group">
              <textarea name="cat_description" id="" cols="30" rows="10" class="form-control text-left" style="text-align: left">
                    {{old('cat_description')}}
              </textarea>
                 <span style="color: red; font-weight: lighter">{{$errors->first('cat_description')}}</span>
            </div>
            <input type="submit" value="Add New Catagory" class="btn btn-primary"/>
        </form>
    </div>
@endsection