@extends('......admin_master')
@section('section')
    <div class="jumbotron col-md-4" style="margin-top:10px; box-shadow: none">
        <h3>Edit Room</h3>
        <form action="{{url('room/editroom')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
            <input type="hidden" name="id" value="{{$room->id}}"/>
           <div class="form-group">
              <input type="text" name="room_no" class="form-control" placeholder="Room number"
              value="@if($errors->all()){{old('room_no')}}@else{{$room->room_no}}@endif"/>
              <span style="color: red; font-weight: lighter">{{$errors->first('room_no')}}</span>
              <span style="color: red">{{session('msg')}}</span>
            </div>
            <div class="form-group">
                 <select name="cat" id="" class="form-control">
                    @foreach($cat as $ca)
                    <option value="{{$ca->id}}">{{$ca->name}}</option>
                    @endforeach
                 </select>
                 <span style="color: red; font-weight: lighter">{{$errors->first('cat')}}</span>
            </div>
            <div class="form-group">
                 <h5>Current Image</h5>
                 <img src="{{url('upload/'.$room->photo)}}" alt="" class="img-responsive" width="150px"/>
                 <hr/>
                 <input type="file" name="photo"/>
                 <span style="color: red; font-weight: lighter">{{$errors->first('photo')}}</span>
                 <span style="color: red">{{session('msg')}}</span>
                 <span style="color: red">{{session('phexit')}}</span>
            </div>
            <input type="submit" value="Edit Room" class="btn btn-primary"/>
        </form>
    </div>
@endsection