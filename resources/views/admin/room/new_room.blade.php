@extends('......admin_master')
@section('section')
    <div class="jumbotron col-md-4" style="margin-top:10px; box-shadow: none">
        <h3>Add new Iteam</h3>
        <form action="{{url('room/newroom')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
            <div class="form-group">
              <input type="text" name="room_no" class="form-control" placeholder="Room number"
               value="@if(session('phexit') && $errors->all()){{old('room_no')}}@endif"/>
              <span style="color: red; font-weight: lighter">{{$errors->first('room_no')}}</span>
            </div>
            <div class="form-group">
                 <select name="cat" id="" class="form-control">
                    @foreach($cat as $ca)
                    <option value="{{$ca->id}}">{{$ca->name}}</option>
                    @endforeach
                 </select>
                 <span style="color: red; font-weight: lighter">{{$errors->first('cat')}}</span>
            </div>
            <div class="form-group">
                 <input type="file" name="photo"/>
                 <span style="color: red; font-weight: lighter">{{$errors->first('photo')}}</span>
                 <p>{{session('msg')}}</p>
                 <p>{{session('phexit')}}</p>
            </div>
            <input type="submit" value="Add New Room" class="btn btn-primary"/>
        </form>
    </div>
@endsection