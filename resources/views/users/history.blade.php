@extends('main_master')
@section('section')
    <span style="color: red">{{session('msg')}}</span>
   <table class="table table-strip">
     <tr style="background-color: darkgray; color: white">
        <th>Room Number</th>
        <th>Check in Date</th>
        <th>Check out Date</th>
        <td>Status</td>
     </tr>

        @foreach($book as $ro)
         <tr>
            <td>{{$ro->room_no}}</td>
            <td>{{$ro->check_in}}</td>
            <td>Rs. {{$ro->check_out}}</td>
            @if($ro->status == 1)
            <td>Booking Cancell</td>
            @endif

         </tr>
        @endforeach

   </table>
@endsection