@extends('main_master')
@section('section')
   <div class="jumbotron col-md-4" style="margin-top:10px; box-shadow: none">
           <h3>Book Now</h3>
           <form action="{{url('ghome/book')}}" method="post">
           {{csrf_field()}}
               <div class="form-group">
                 <input type="hidden" name="email" value="{{$email}}"/>
                 <input type="hidden" name="room_no" value="{{$room_no}}"/>
               </div>
               <div class="form-group">
                   <input type="date" name="check_in" class="form-control" value="{{old('cat_price')}}"/>
                   <span style="color: red; font-weight: lighter">{{$errors->first('check_in')}}</span>
                   <span style="color: red">{{session('date')}}</span>
               </div>
               <div class="form-group">
                  <input type="date" name="check_out" class="form-control" value="{{old('cat_price')}}"/>
                  <span style="color: red; font-weight: lighter">{{$errors->first('check_out')}}</span>
              </div>
               <input type="submit" value="Book Now" class="btn btn-primary"/>
           </form>
       </div>
@endsection