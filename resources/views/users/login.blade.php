@extends('../main_master')
@section('section')
    <div class="jumbotron col-md-4" style="margin-top:10px; box-shadow: none">
        <h3>Login</h3>
        <form action="{{url('guest/login')}}" method="post">
        {{csrf_field()}}

            <div class="form-group">
              <input type="email" name="email" class="form-control" placeholder="email" value="{{old('email')}}"/>
              <span style="color: red; font-weight: lighter">{{$errors->first('email')}}</span>
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="Password"/>
              <span style="color: red; font-weight: lighter">{{$errors->first('password')}}</span>
            </div>
            <input type="submit" value="Login" class="btn btn-info"/>
        </form>
    </div>
@endsection