@extends('main_master')
@section('section')
    <span style="color: red">{{session('msg')}}</span>
   <table class="table table-strip">
     <tr style="background-color: darkgray; color: white">
        <th>Room Number</th>
        <th>Room Catagory</th>
        <th>Room Price</th>
        <th>Room Description</th>
        <th>Room Photo</th>
        <td>Action</td>
     </tr>

        @foreach($room as $ro)
         <tr>
            <td>{{$ro->room_no}}</td>
            <td>{{$ro->cat->name}}</td>
            <td>Rs. {{$ro->cat->price}}</td>
            <td>{{$ro->cat->description}}</td>
            <td><img src="{{url('upload/'.$ro->photo)}}" alt="" class="img-responsive" width="100px"/></td>
            <td>
            @if(\App\Room::where('room_no', $ro->room_no)->value('status') == 1)
                <span style="background-color: #000000; color:white; padding: 7px 10px;">Booked</span>
                <a href="{{url('ghome/cancel/'.$ro->room_no)}}" class="btn btn-sm btn-danger">Cancell Booking</a>
            @else
            <a href="{{url('ghome/book/'.$ro->room_no)}}" class="btn btn-sm btn-info">Book now</a>
            @endif

            </td>
         </tr>
        @endforeach

   </table>
@endsection