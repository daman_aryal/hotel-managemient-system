@extends('../main_master')
@section('section')
    <div class="jumbotron col-md-4" style="margin-top:10px; box-shadow: none">
        <h3>Create Your Account</h3>
        <form action="{{url('guest/signup')}}" method="post">
        {{csrf_field()}}
            <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Name" value="{{old('name')}}"/>
                  <span style="color: red; font-weight: lighter">{{$errors->first('name')}}</span>
                </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" placeholder="Email" value="{{old('email')}}"/>
              <span style="color: red; font-weight: lighter">{{$errors->first('email')}}</span>
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="Password"/>
              <span style="color: red; font-weight: lighter">{{$errors->first('password')}}</span>
            </div>
            <div class="form-group">
              <input type="text" name="address" class="form-control" placeholder="Address" value="{{old('address')}}"/>
              <span style="color: red; font-weight: lighter">{{$errors->first('address')}}</span>
            </div>
            <div class="form-group">
              <input type="text" name="phoneno" class="form-control" placeholder="Phone" value="{{old('phoneno')}}"/>
              <span style="color: red; font-weight: lighter">{{$errors->first('phoneno')}}</span>
            </div>
             <input type="submit" value="Sign Up" class="btn btn-info"/>
        </form>
    </div>
@endsection