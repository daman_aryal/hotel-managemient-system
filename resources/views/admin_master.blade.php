<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>$title</title>
<link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{url('css/material.min.css')}}"/>
</head>
<body>
    <div class="container">
            @if(Auth::user())
            <nav class="nav nav-bar">
                <ul class="nav navbar-nav navbar-default">
                    <li><a href="{{url('cat/dash')}}">Dash</a></li>
                    <li><a href="{{url('cat/showcat')}}">Room Catagory Mgmt</a></li>
                    <li><a href="{{url('room/showroom')}}">Room Mgmt</a></li>
                    <li><a href="{{url('ahome/logout')}}">Log out</a></li>
                 </ul>
            </nav>
            @endif
            @yield('section')
    </div>

</body>
</html>