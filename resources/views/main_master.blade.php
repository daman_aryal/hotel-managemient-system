<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>$title</title>
<link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{url('css/material.min.css')}}"/>
</head>
<body>
    <div class="container">
            <nav class="nav nav-bar">
                <ul class="nav navbar-nav navbar-default">
                        @if(Auth::user())
                        <li><a href="{{url('ghome/home')}}">Home</a></li>
                        <li><a href="{{url('ghome/history')}}">History</a></li>
                        <li><a href="{{url('ghome/logout')}}">Logout</a></li>
                        @else
                        <li><a href="{{url('guest/signup')}}">Sign Up</a></li>
                        <li><a href="{{url('guest/login')}}">Login</a></li>
                        @endif
                </ul>
            </nav>
            @yield('section')
    </div>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script src="{{url('js/')}}"></script>
</body>
</html>