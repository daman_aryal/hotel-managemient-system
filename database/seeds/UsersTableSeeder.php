<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $data = array(
            'name'=>'Daman',
            'email'=>'daman19dig@gmail.com',
            'password'=>Hash::make('daman'),
            'address'=>'Samakhusi',
            'phoneno'=>'9849754423',
            'role'=>'1'
        );
        DB::table('users')->insert($data);
    }
}
