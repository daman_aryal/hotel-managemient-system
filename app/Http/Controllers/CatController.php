<?php

namespace App\Http\Controllers;

use App\Cat;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CatController extends Controller
{
    private $cat;
    public function  __construct(Cat $cat){
        $this->cat = $cat;
    }

    public function getNewcat(){
        $title = "New Catagory";
        return view('admin.cat.new_cat')->with('title', $title);
    }

    public function postNewcat(Request $request){
        $this->validate($request,
            [
                'name' => 'required|alpha_dash|unique:cats',
                'cat_price' => 'required|numeric',
                'cat_description' => 'required'
            ]
            );

        $this->cat->name =$request->get('name');
        $this->cat->price =$request->get('cat_price');
        $this->cat->description =$request->get('cat_description');
        $this->cat->save();

        return redirect('cat/showcat');
    }

    
    public function getShowcat(){
        $title = "show cat";
        $cat = $this->cat->all();
        return view('admin.cat.show_cat')->with('title', $title)
            ->with('cat', $cat);
    }

    public function getEditcat($id){
        $eData = $this->cat->find($id);
        $title = "Edit cat";
        return view('admin.editcat')
            ->with('eData', $eData)
            ->with('title', $title);
    }

    public function postEditcat(Request $request){
        $this->validate($request,
            [
                'cat_name' => 'required|alpha_dash',
                'cat_price' => 'required|numeric',
                'cat_description' => 'required'
            ]
        );

        $exit = $this->cat->where('name', $request->get('cat_name'))->where('id', '!=', $request->get('id'))->exists();
        if($exit){
            return Redirect('cat/editcat/'.$request->get('id'))->with(['msg'=>'catagory name already exit']);
        }

        $updateCat = $this->cat->find($request->get('id'));
        $updateCat->name = $request->get('cat_name');
        $updateCat->price = $request->get('cat_price');
        $updateCat->description = $request->get('cat_description');
        $updateCat->save();

        return Redirect('cat/showcat');
    }

    public function getDelcat($id){
       $this->cat->find($id)->delete();
        return Redirect('cat/showcat');
    }
}
