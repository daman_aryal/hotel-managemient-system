<?php

namespace App\Http\Controllers;

use App\Cat;
use App\Guest;
use App\Room;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class GuestsController extends Controller
{
    public function __construct(User $user){
        return $this->user = $user;
    }
    public function getSignup(){
        $title = "Sign up";
        return view('users.signup')
            ->with('title', $title);
    }

    public function postSignup(Request $request){
        $this->validate($request,
            [
                /*'fname'=>'required|alpha',
                'lname'=>'required|alpha',
                'username'=>'required|alpha_dash|min:3|max:15|unique:guests',*/
                'name'=>'required',
                'email'=>'required|email|unique:users',
                'password'=>'required',
                'address'=>'required',
                'phoneno'=>'required|numeric|digits:10|unique:users'
            ]);

        /*$this->guest->fname = $request->get('fname');
        $this->guest->lname = $request->get('lname');
        $this->guest->username = $request->get('username');*/
        $this->user->name = $request->get('name');
        $this->user->email = $request->get('email');
        $this->user->password = Hash::make($request->get('password'));
        $this->user->address = $request->get('address');
        $this->user->phoneno = $request->get('phoneno');
        $this->user->role = "2";
        $this->user->save();

        return Redirect('guest/login');
    }

    public function getLogin(){
        $title = "Guest Login";
        return view('users.login')
            ->with('title', $title);
    }

    public function postLogin(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required'
        ]);

                $crediantal = array(
                            'email'=>$request->get('email'),
                            'password'=>$request->get('password')
                        );

                        if(Auth::attempt($crediantal)){
                            return Redirect('ghome/guestdash');
                        }else{
                            return Redirect('guest/login');
                        }

                /*$username = $this->guest->where('password', Hash::make($request->get('password')))->get();
                if($username->count() > 0){
                    dd('successfully login');
                }else{
                    dd('unsuccess login');
                }*/
        }



}
