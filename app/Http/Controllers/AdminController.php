<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function getLogin(){
        $title = "Admin Login";
        return view('admin.login')->with('title', $title);
    }

    public function postLogin(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'password'=>'required'
        ]);

        $crediatial = array(
            'email'=>$request->get('email'),
            'password'=>$request->get('password')
        );



        if(Auth::attempt($crediatial)){
            return redirect('ahome/dash');
        }else{
            return redirect('admin/login');
        }
    }


}
