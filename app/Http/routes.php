<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

    Route::get('/', function () {
        $cat = \App\Cat::all();
        $room = \App\Room::all();
        $title = "main page";
        return view('main')
            ->with('cat', $cat)
            ->with('room', $room)
            ->with('title', $title);
    });

    Route::controller('admin', 'AdminController');

    Route::group(
        ['middleware'=>'AdminMid'],
        function(){
            Route::controller('ahome', 'AdminHomeController');
            Route::controller('cat', 'CatController');
            Route::controller('room', 'RoomController');
        }
    );

    Route::controller('guest', 'GuestsController');
    Route::group(
        ['middleware'=>'UserMid'],
        function(){
            Route::controller('ghome', 'GuestHomeController');
        }
    );

